# Passo a passo Hands-on kind (**K**ubernetes **IN** **D**ocker)

Este é um passo-a-passo para a instalação e configuração do kind (**K**ubernetes **IN** **D**ocker) e o deploy da uma aplicação simples em Golang.

Sequência de passos:

1. [Instalação do docker](https://gitlab.com/joellgarcia/workshop-k8s#instala%C3%A7%C3%A3o-do-docker)
1. [Instalação do kubectl](https://gitlab.com/joellgarcia/workshop-k8s#instala%C3%A7%C3%A3o-do-kubectl)
1. [Instalação do Kind](https://gitlab.com/joellgarcia/workshop-k8s#instala%C3%A7%C3%A3o-do-kind)
1. [Habilitando os recursos instalados](https://gitlab.com/joellgarcia/workshop-k8s#habilitando-os-recursos-instalados)
1. [Clonando o repositório](https://gitlab.com/joellgarcia/workshop-k8s#clonando-o-reposit%C3%B3rio)
1. [Criação do cluster](https://gitlab.com/joellgarcia/workshop-k8s#cria%C3%A7%C3%A3o-do-cluster)
1. [Deploy do Nginx Ingress Controller no cluster](https://gitlab.com/joellgarcia/workshop-k8s#deploy-do-nginx-ingress-controller-no-cluster)
1. [Deploy da aplicação hello-world-go](https://gitlab.com/joellgarcia/workshop-k8s#deploy-da-aplica%C3%A7%C3%A3o-hello-world-go)
1. [Criando o Namespace](https://gitlab.com/joellgarcia/workshop-k8s#criando-o-namespace)
1. [Criando o Deploy](https://gitlab.com/joellgarcia/workshop-k8s#criando-o-deploy)
1. [Verificando os Pods](https://gitlab.com/joellgarcia/workshop-k8s#verificando-os-pods)
1. [Criando o Service](https://gitlab.com/joellgarcia/workshop-k8s#criando-o-service)
1. [Verificando o Service](https://gitlab.com/joellgarcia/workshop-k8s#verificando-o-service)
1. [Criando o Ingress para o Service](https://gitlab.com/joellgarcia/workshop-k8s#criando-o-ingress-para-o-service)
1. [Verificando o Ingress](https://gitlab.com/joellgarcia/workshop-k8s#verificando-o-ingress)
1. [Testando a aplicação](https://gitlab.com/joellgarcia/workshop-k8s#testando-a-aplica%C3%A7%C3%A3o)
1. [Links Úteis](https://gitlab.com/joellgarcia/workshop-k8s#links-%C3%BAteis)


### Instalação do docker
```
sudo apt install docker.io -y
```
```
docker -v
```
```
sudo usermod -aG docker ${USER}
```
### Instalação do kubectl
```
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
```
```
chmod +x ./kubectl
```
```
sudo mv ./kubectl /usr/local/bin/kubectl
```
```
kubectl version --client
```
```
kubectl completion bash | sudo tee /etc/bash_completion.d/kubectl > /dev/null
```
### Instalação do kind
```
curl -Lo ./kind https://github.com/kubernetes-sigs/kind/releases/download/v0.20.0/kind-linux-amd64
```
```
chmod +x ./kind
```
```
sudo mv ./kind /usr/local/bin/kind
```
```
kind version
```
```
kind completion bash | sudo tee /etc/bash_completion.d/kind > /dev/null
```
### Habilitando os recursos instalados
```
sudo su - ${USER}
```
### Clonando o repositório
```
git clone --recurse-submodules https://gitlab.com/joellgarcia/workshop-k8s.git
```
```
cd workshop-k8s
```
### Criação do cluster
```
kind create cluster --config kind-config.yaml
```
```
docker container ls
```
```
kubectl get nodes -o wide
```
### Deploy do Nginx Ingress Controller no cluster
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
```
## Deploy da aplicação hello-world-go
### Criando o namespace
```
kubectl apply -f hello-world-go-namespace.yaml
```
### Criando o Deploy
```
kubectl apply -f hello-world-go-deployment.yaml
```
### Verificando os Pods
```
kubectl get pods -n hello-world-go
```
### Criando o Service
```
kubectl apply -f hello-world-go-service.yaml
```
### Verificando o Service
```
kubectl get svc -o wide -n hello-world-go
```
### Criando o Ingress para o Service
```
kubectl apply -f hello-world-go-ingress.yaml
```
### Verificando o Ingress
```
kubectl get ingress -o wide -n hello-world-go
```
### Testando a aplicação
```
curl localhost:8080/hello-world-go/
```
[http://localhost:8080/hello-world-go/](http://localhost:8080/hello-world-go/)
```
curl localhost:8080/hello-world-go/ping
```
[http://localhost:8080/hello-world-go/ping](http://localhost:8080/hello-world-go/ping)

</br><br>
---
</br><br>

# Links úteis

[Slides](https://docs.google.com/presentation/d/1Dk5_O4zqfkBtEz22vORrdKVlL9xQE2zDYg5tks-W4jA/edit?usp=sharing)

## Documentação

[https://docs.docker.com/](https://docs.docker.com/)

[https://kubernetes.io/docs/home/](https://kubernetes.io/docs/home/)

[https://kind.sigs.k8s.io/](https://kind.sigs.k8s.io/)

## Treinamentos grátis LinuxTips

[https://www.linuxtips.io/course/linux-essentials](https://www.linuxtips.io/course/linux-essentials)

[https://www.linuxtips.io/course/docker-essentials](https://www.linuxtips.io/course/docker-essentials)

[https://www.linuxtips.io/course/kubernetes-essentials](https://www.linuxtips.io/course/kubernetes-essentials)

[https://www.youtube.com/linuxtips](https://www.youtube.com/linuxtips)

## Livros Grátis

[https://livro.descomplicandodocker.com.br/](https://livro.descomplicandodocker.com.br/)

[https://livro.descomplicandokubernetes.com.br/](https://livro.descomplicandokubernetes.com.br/)
